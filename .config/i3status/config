#  _   _ _ _         _            ______            _         _            
# | \ | (_) |       | |           | ___ \          | |       | |           
# |  \| |_| | _____ | | __ _ _   _| |_/ / __ _ _ __| | ____ _| | _____   __
# | . ` | | |/ / _ \| |/ _` | | | | ___ \/ _` | '__| |/ / _` | |/ _ \ \ / /
# | |\  | |   < (_) | | (_| | |_| | |_/ / (_| | |  |   < (_| | | (_) \ V / 
# \_| \_/_|_|\_\___/|_|\__,_|\__, \____/ \__,_|_|  |_|\_\__,_|_|\___/ \_/  
#                            __/ |                                        
#                           |___/      
# Install awesome-terminal-fonts awesome-fonts
general {
    colors = true
    interval = 1
    color_good = "#2AA198"
    color_bad = "#DC322F"
    color_degraded = "#fe8019"
    output_format = i3bar
}

order += "disk /"
order += "run_watch openvpn"
order += "run_watch openconnect"
order += "wireless _first_"
order += "cpu_temperature 0"
order += "cpu_usage"
order += "memory"
order += "battery all"
order += "volume master"
order += "tztime local"

disk "/" {
    format = "  NikolayBarkalov "
}

run_watch openvpn {
    pidfile = "/var/run/openvpn.pid"
    format = " openvpn"
    format_down=""
}

run_watch openconnect {
    pidfile = "/var/run/openconnect.pid"
    format = " openconnect vpn"
    format_down=""
}

wireless _first_ {
    format_up = "%quality %frequency %essid %ip"
    format_down = "W: down"
}

cpu_temperature 0 {
    format = " %degrees°c"
    max_threshold = "70"
    format_above_threshold = " %degrees"
    path = "/sys/class/thermal/thermal_zone0/temp"
}

cpu_usage {
    format = " %usage "
    degraded_threshold = "80"
    max_threshold = "90"
}

memory {
    format = "📏  %used "
}

battery all {
    format = " %status %percentage "
        format_down = "No battery"
        last_full_capacity = true
        integer_battery_capacity = true
        status_chr = ""
        status_bat = ""
        status_unk = ""
        status_full = ""
        low_threshold = 30
        threshold_type = time
}

volume master {
    format = "  %volume "
    format_muted = " mute "
    device = "default"
    mixer = "Master"
    mixer_idx = 0
}

tztime local {
    format = " %A  %d %B  %H:%M |"
}
